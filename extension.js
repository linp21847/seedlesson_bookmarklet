/************************************************************************************
  This is your Page Code. The appAPI.ready() code block will be executed on every page load.
  For more information please visit our wiki site: http://docs.crossrider.com
*************************************************************************************/

/*
 * Description:
 *   This extension uses API methods that run in the background scope. To view the code, see background.js.
 *
 * Usage:
 *   Runs in the browser's background.
 *
 * Reference:
 *   http://docs.crossrider.com/#!/api/appAPI.browserAction
 */

appAPI.ready(function($) {

    // Place your code here (you can also define new functions above this scope)
    // The $ object is the extension's jQuery object
    appAPI.message.addListener(function(msg) {
	    if (msg.type === 'urlFeed') {
			// appAPI.request.post({
			//     url: 'REST API URL',
			//     postData: dataToSend,
			//     onSuccess: function(response, additionalInfo) {
			//         var details = {};
			//         details.response = response;
			//     },
			//     onFailure: function(httpCode) {
			//       //  alert('POST:: Request failed. HTTP Code: ' + httpCode);
			//     }
			// });   
			console.log(document.location.href);
			window.location.href='https://www.seedlesson.com/resource/clip?url='+encodeURIComponent(location.href)+'&resource='+encodeURIComponent(document.title);
	    }
	 });
});