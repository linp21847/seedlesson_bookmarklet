/************************************************************************************
  This is your background code.
  For more information please visit our wiki site:
  http://docs.crossrider.com/#!/guide/background_scope
*************************************************************************************/

/*
 * Description:
 *   This extension demonstrates how to work with the Browser Action button.
 *
 * Usage:
 *   Runs in the browser's background.
 *   Click on the button and watch it change
 *
 * Reference:
 *   http://docs.crossrider.com/#!/api/appAPI.browserAction
 */

// Place your code here (ideal for handling browser button, global timers, etc.)

// Note: the $ variable that represents the jQuery object is not available
//       in the background scope
appAPI.ready(function() {
	// Global variable to hold the toggle state of the button
	var buttonState = true;
	
	// Sets the initial browser icon
	appAPI.browserAction.setResourceIcon('images/favicon.png');
	
	// Sets the tooltip for the button
	appAPI.browserAction.setTitle('Seedlession Clipper');
	
	// Sets the text and background color for the button
	if (appAPI.browser.name !== 'safari') {
		// appAPI.browserAction.setBadgeText('Icon');
		// appAPI.browserAction.setBadgeBackgroundColor([255,0,0,50]);
	}
	else
		// Safari only supports numeric characters
		// and has a fixed background color
		appAPI.browserAction.setBadgeText('1234');
	
	// Sets the initial onClick event handler for the button
	appAPI.browserAction.onClick(function(){
		if (buttonState) {
			if (appAPI.browser.name !== 'safari') {
				// Sets the text and background color for the button
				// using the optional background parameter
				// appAPI.browserAction.setBadgeText('Xrdr', [0,0,255,255]);
				// Sets the icon to use for the button.
				appAPI.browserAction.setResourceIcon('images/favicon.png');
			} else
				// Safari only supports numeric characters,
				// has a fixed background color,
				// and can only use the extension's icon
				// appAPI.browserAction.setBadgeText('4321');
				console.log("Safari browser was opened.");
		} else {
			// Remove the badge from the button
			// appAPI.browserAction.removeBadge();
			
			if (appAPI.browser.name !== 'safari')
				// Reset the icon for the image
				appAPI.browserAction.setResourceIcon('images/favicon.png');
		}
		
		// Toggle the state
		buttonState = !buttonState;
		
		appAPI.message.toActiveTab({type:'urlFeed'});
	});
});